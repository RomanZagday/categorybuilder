﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using codefirst.Models;
using codefirst.Models.ViewModels;


namespace codefirst.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            HomeViewModel model = new HomeViewModel();
            DataContext db = new DataContext();

            var listCategories= db.Category.ToList();

            foreach (var item in listCategories)
            {
                model.Categories.Add(
                new CategoriesViewModel
                {
                    Id = item.Id,
                    Title = item.Title
                });
            }

            var listFilters = db.Filters.ToList();
            foreach (var item in listFilters)
            {
                model.Filters.Add(
                new FilterViewModel
                {
                    Id = item.Id,
                    Title = item.Title,
                    Type = item.Type
                });
            }

            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}