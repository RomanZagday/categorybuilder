﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace codefirst.Models.DataModels
{
    public class Filter
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int Type { get; set; }

        public List<Category> categories { get; set; }
    }
}