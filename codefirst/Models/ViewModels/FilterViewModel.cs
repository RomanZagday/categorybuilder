﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace codefirst.Models.ViewModels
{
    public class FilterViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int Type { get; set; }

    }
}