﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace codefirst.Models.ViewModels
{
    public class HomeViewModel
    {

        public HomeViewModel()
        {
            Categories = new List<CategoriesViewModel>();
            Filters = new List<FilterViewModel>();

        }

        public List<CategoriesViewModel> Categories;
        public List<FilterViewModel> Filters;

        
    }
   
}