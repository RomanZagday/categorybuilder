﻿using codefirst.Models.DataModels;
namespace codefirst.Models
{
    using System.Data.Entity;
    

    public class DataContext : DbContext
    {
     
        public DataContext()
            : base("name=Model1")
        {
        }
        public DbSet<Category> Category { get; set; }
        public DbSet<Filter> Filters { get; set; }
    }
}