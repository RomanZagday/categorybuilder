namespace codefirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class start : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Category_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.Category_Id)
                .Index(t => t.Category_Id);
            
            CreateTable(
                "dbo.Filters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FilterCategories",
                c => new
                    {
                        Filter_Id = c.Int(nullable: false),
                        Category_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Filter_Id, t.Category_Id })
                .ForeignKey("dbo.Filters", t => t.Filter_Id, cascadeDelete: true)
                .ForeignKey("dbo.Categories", t => t.Category_Id, cascadeDelete: true)
                .Index(t => t.Filter_Id)
                .Index(t => t.Category_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FilterCategories", "Category_Id", "dbo.Categories");
            DropForeignKey("dbo.FilterCategories", "Filter_Id", "dbo.Filters");
            DropForeignKey("dbo.Categories", "Category_Id", "dbo.Categories");
            DropIndex("dbo.FilterCategories", new[] { "Category_Id" });
            DropIndex("dbo.FilterCategories", new[] { "Filter_Id" });
            DropIndex("dbo.Categories", new[] { "Category_Id" });
            DropTable("dbo.FilterCategories");
            DropTable("dbo.Filters");
            DropTable("dbo.Categories");
        }
    }
}
