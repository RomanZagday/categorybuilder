namespace codefirst.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<codefirst.Models.DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(codefirst.Models.DataContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //





            context.Category.Add(new Models.DataModels.Category { Title = "c1" });
            context.Category.Add(new Models.DataModels.Category { Title = "c2" });
            context.Category.Add(new Models.DataModels.Category { Title = "c3" });

            context.Filters.Add(new Models.DataModels.Filter { Title = "f1" });
            context.Filters.Add(new Models.DataModels.Filter { Title = "f2" });
            context.Filters.Add(new Models.DataModels.Filter { Title = "f3" });


            context.SaveChanges();
        }
    }
}
